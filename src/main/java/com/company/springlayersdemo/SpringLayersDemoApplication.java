package com.company.springlayersdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLayersDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLayersDemoApplication.class, args);
    }
}
