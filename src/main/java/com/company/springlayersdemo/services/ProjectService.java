package com.company.springlayersdemo.services;

import com.company.springlayersdemo.models.Project;

import java.util.List;

public interface ProjectService {
    void create(Project project);
    List<Project> getAll();
    Project getById(int id);
    Project update(int id, Project project);
    void delete(int id);
}
