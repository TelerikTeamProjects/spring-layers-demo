package com.company.springlayersdemo.services;

import com.company.springlayersdemo.models.Employee;
import com.company.springlayersdemo.models.Project;
import com.company.springlayersdemo.repositories.MainRepository;
import com.company.springlayersdemo.repositories.SQLRepository;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private MainRepository repository;
    // private SQLRepository repository;

  /*  @Autowired
    public EmployeeServiceImpl(SQLRepository repository) {
        this.repository = repository;
    }*/

    @Autowired
    public EmployeeServiceImpl( @Qualifier("repository") MainRepository repository) {
        this.repository = repository;
    }

    @Override
    public void create(Employee employee) {
        Map<String, String> filters = new HashMap<>();
        filters.put("firstName", employee.getFirstName());
        filters.put("lastName", employee.getLastName());
        List<Employee> employees = getFilteredEmployees(filters);
        /*repository.getAllEmployees().stream()
                .filter(employee1 -> employee1.getFirstName().equals(employee.getFirstName())
                        && employee1.getLastName().equals(employee.getLastName()))
                .collect(Collectors.toList());
        Collectors.toList();*/

        if (employees.size() > 0) {
            throw new IllegalArgumentException(String.format("Employee with first name %s and last name %s found.",
                    employee.getFirstName(), employee.getLastName()));
        }
        repository.create(employee);
    }

    @Override
    public List<Employee> getFilteredEmployees(Map<String, String> filters) {
        List<Employee> result = repository.getAllEmployees();

        if (filters.get("firstName") != null) {
            result = result.stream()
                    .filter(e -> e.getFirstName().equals(filters.get("firstName")))
                    .collect(Collectors.toList());
        }

        if (filters.get("lastName") != null) {
            result = result.stream()
                    .filter(e -> e.getLastName().equals(filters.get("lastName")))
                    .collect(Collectors.toList());
        }

        return result;
    }

    @Override
    public Employee getById(int id) {
        return getByIdHelper(id);
    }

    @Override
    public Employee update(int id, Employee employee) {
        Employee employeeToUpdate;
        employeeToUpdate = repository.getAllEmployees().stream()
                .filter(e -> e.getId() == id)
                .findFirst()
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, String.format("Employee with id %d not found.", id)));
        return repository.update(employeeToUpdate, employee);
    }

    @Override
    public Employee addProject(int id, int projectId) {
        Employee employeeToUpdate = getByIdHelper(id);
        Project project = repository.getAllProjects().stream().filter(p -> p.getId() == projectId).findFirst()
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Project with id %d not found.", id)));

        employeeToUpdate.getEmployeeProjects().add(project);
        project.getProjectMembers().add(employeeToUpdate);

        return employeeToUpdate;
    }

    @Override
    public void removeProject(int employeeId, int projectId) {
        Employee employeeToUpdate = getByIdHelper(employeeId);
        Project project = repository.getAllProjects().stream().filter(p -> p.getId() == projectId).findFirst()
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Project with id %d not found.", projectId)));
        employeeToUpdate.getEmployeeProjects().remove(project);
        project.getProjectMembers().remove(employeeToUpdate);
    }

    @Override
    public void delete(int id) {
        Employee employeeToDelete = getByIdHelper(id);
        for (Project project : employeeToDelete.getEmployeeProjects()) {
            project.getProjectMembers().remove(employeeToDelete);
        }
        repository.delete(employeeToDelete);
    }

    private Employee getByIdHelper(int id) {
        return repository.getAllEmployees().stream()
                .filter(e -> e.getId() == id)
                .findFirst()
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Employee with id %d not found.", id)));
    }
}
