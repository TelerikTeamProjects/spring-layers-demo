package com.company.springlayersdemo.services;

import com.company.springlayersdemo.models.Employee;

import java.util.List;
import java.util.Map;

public interface EmployeeService {
    void create(Employee employee);
    List<Employee> getFilteredEmployees(Map<String, String> filters);
    Employee getById(int id);
    Employee update(int id, Employee employee);
    Employee addProject(int id, int projectId);
    void removeProject(int employeeId, int projectId);
    void delete(int id);
}
