package com.company.springlayersdemo.services;

import com.company.springlayersdemo.models.Employee;
import com.company.springlayersdemo.models.Project;
import com.company.springlayersdemo.repositories.MainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {
    private MainRepository repository;

    @Autowired
    public ProjectServiceImpl( @Qualifier("repository") MainRepository repository) {
        this.repository = repository;
    }

    @Override
    public void create(Project project) {
        repository.create(project);
    }

    @Override
    public List<Project> getAll() {
        return repository.getAllProjects();
    }

    @Override
    public Project getById(int id) {
        return getByIdHelper(id);
    }

    @Override
    public Project update(int id, Project project) {
        Project projectToUpdate = getByIdHelper(id);
        return repository.update(projectToUpdate, project);
    }

    @Override
    public void delete(int id) {
        Project projectToDelete = getByIdHelper(id);
        for (Employee employee : repository.getAllEmployees()) {
            employee.getEmployeeProjects().remove(getByIdHelper(id));
        }
        repository.delete(projectToDelete);
    }

    private Project getByIdHelper(int id) {
        return repository.getAllProjects().stream()
                .filter(e -> e.getId() == id)
                .findFirst()
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Project with id %d not found.", id)));
    }
}
