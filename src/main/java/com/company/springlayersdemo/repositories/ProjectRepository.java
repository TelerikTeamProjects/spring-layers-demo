package com.company.springlayersdemo.repositories;

import com.company.springlayersdemo.models.Project;

import java.util.List;

public interface ProjectRepository {
    void create(Project project);
    List<Project> getAllProjects();
    Project update(Project projectToUpdate, Project project);
    void delete(Project project);
}
