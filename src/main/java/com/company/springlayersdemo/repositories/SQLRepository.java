package com.company.springlayersdemo.repositories;

import com.company.springlayersdemo.models.Employee;
import com.company.springlayersdemo.models.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository("sql_repository")
@PropertySource("classpath:application.properties")
public class SQLRepository implements EmployeeRepository, ProjectRepository {
    private List<Employee> employees;
    private List<Project> projects;

    private String dbUrl, dbUser, dbPassword;

    @Autowired
    public SQLRepository(Environment env) {
        dbUrl = env.getProperty("database.url");
        dbUser = env.getProperty("database.username");
        dbPassword = env.getProperty("database.password");
    }

    @Override
    public void create(Employee employee) {
       throw new NotImplementedException();
    }

    @Override
    public List<Employee> getAllEmployees() {
        try {
            System.out.println("Open connection :)");
            Connection connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);

            Statement statement = connection.createStatement();
            String query = "select EmployeeID, FirstName, LastName from telerikacademy.employees";
            ResultSet resultSet = statement.executeQuery(query);

            return processResultSet(resultSet);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            // e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public Employee update(Employee employeeToUpdate, Employee employee) {
        throw new NotImplementedException();
    }

    @Override
    public void delete(Employee employee) {
        throw new NotImplementedException();
    }

    @Override
    public void create(Project project) {
        throw new NotImplementedException();
    }

    @Override
    public List<Project> getAllProjects() {
        throw new NotImplementedException();
    }


    @Override
    public Project update(Project projectToUpdate, Project project) {
        throw new NotImplementedException();
    }

    @Override
    public void delete(Project project) {
        throw new NotImplementedException();
    }

    private List<Employee> processResultSet(ResultSet employeesData) throws SQLException {
        List<Employee> employees = new ArrayList<>();
        while (employeesData.next()) {
            Employee e = new Employee(employeesData.getInt("EmployeeID"),
                    employeesData.getString("FirstName"),
                    employeesData.getString("LastName"));
                   // employeesData.getString("JobTitle"));
            employees.add(e);
        }
        return employees;
    }

}
