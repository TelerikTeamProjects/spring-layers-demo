package com.company.springlayersdemo.repositories;

import com.company.springlayersdemo.models.Employee;

import java.util.List;

public interface EmployeeRepository {
    void create(Employee employee);
    List<Employee> getAllEmployees();
    Employee update(Employee employeeToUpdate, Employee employee);
    void delete(Employee employee);
}
