package com.company.springlayersdemo.repositories;

import com.company.springlayersdemo.models.Employee;
import com.company.springlayersdemo.models.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository("repository")
public class MainRepository implements EmployeeRepository, ProjectRepository {
    private List<Employee> employees;
    private List<Project> projects;

    @Autowired
    public MainRepository() {
        employees = new ArrayList<>(Arrays.asList(new Employee(1, "First Name", "Last Name")));
        projects = new ArrayList<>();
    }

    @Override
    public void create(Employee employee) {
        employees.add(employee);
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employees;
    }

    @Override
    public Employee update(Employee employeeToUpdate, Employee employee) {
        employeeToUpdate.setFirstName(employee.getFirstName());
        employeeToUpdate.setLastName(employee.getLastName());
        return employeeToUpdate;
    }

    @Override
    public void delete(Employee employee) {
        employees.remove(employee);
    }

    @Override
    public void create(Project project) {
        projects.add(project);
    }

    @Override
    public List<Project> getAllProjects() {
        return projects;
    }


    @Override
    public Project update(Project projectToUpdate, Project project) {
        projectToUpdate.setName(project.getName());
        return projectToUpdate;
    }

    @Override
    public void delete(Project project) {
        projects.remove(project);
    }
}
