package com.company.springlayersdemo.controllers;

import com.company.springlayersdemo.models.Project;
import com.company.springlayersdemo.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/projects")
public class ProjectController {
    private ProjectService service;

    @Autowired
    public ProjectController(ProjectService service) {
        this.service = service;
    }

    @PostMapping("/new")
    public Project create(@Valid @RequestBody Project project) {
        service.create(project);
        return project;
    }

    @GetMapping
    public List<Project> getProjects(){
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Project getById(@Valid @PathVariable int id) {
        try {
            return service.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Project with id %d not found.", id));
        }
    }

    @PutMapping("/{id}")
    public Project update(@Valid @PathVariable int id, @Valid @RequestBody Project project) {
        return service.update(id, project);
    }

    @DeleteMapping("/{id}")
    public void delete(@Valid @PathVariable int id) {
        service.delete(id);
    }
}
