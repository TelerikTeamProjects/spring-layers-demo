package com.company.springlayersdemo.controllers;

import com.company.springlayersdemo.models.Employee;
import com.company.springlayersdemo.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {
    private EmployeeService service;

    @Autowired
    public EmployeeController(EmployeeService service) {
        this.service = service;
    }

    @PostMapping("/new")
    public Employee create(@Valid @RequestBody Employee employee) {
        try {
            service.create(employee);
            return employee;
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Employee with first name %s and last name %s already exists.",
                    employee.getFirstName(), employee.getLastName()));
        }
    }

    @GetMapping
    public List<Employee> getEmployees(@RequestParam(required = false) String firstName,
                                       @RequestParam(required = false) String lastName) {

        Map<String, String> filters = new HashMap<>();
        filters.put("firstName", firstName);
        filters.put("lastName", lastName);
        return service.getFilteredEmployees(filters);
    }

    @GetMapping("/{id}")
    public Employee getById(@Valid @PathVariable int id) {
        try {
            return service.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Employee with id %d not found.", id));
        }
    }

    @PutMapping("/{id}")
    public Employee update(@Valid @PathVariable int id, @Valid @RequestBody Employee employee) {
        try {
            return service.update(id, employee);
        }catch(IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Employee with id %d not found.", id));
        }
    }

    @PutMapping("/{id}/projects/{projectId}")
    public Employee addProject(@Valid @PathVariable int id, @Valid @PathVariable int projectId) {
       return service.addProject(id, projectId);
    }

    @DeleteMapping("/{id}/projects/{projectId}")
    public void removeProject(@Valid @PathVariable int employeeId, @Valid @PathVariable int projectId){
        service.removeProject(employeeId, projectId);
    }

    @DeleteMapping("/{id}")
    public void delete(@Valid @PathVariable int id) {
        service.delete(id);
    }
}
