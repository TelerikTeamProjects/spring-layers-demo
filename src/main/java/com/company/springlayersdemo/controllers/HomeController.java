package com.company.springlayersdemo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;
import java.util.List;

@Controller
public class HomeController {
    private EmployeeController employeeController;

    @Autowired
    public HomeController(EmployeeController employeeController) {
        this.employeeController = employeeController;
    }

    // @Autowired is the same, some methods return Strings
    // return view name as "index" - relative path searched in templates

    @GetMapping("/")
    public String index(){
        List<String> test = Arrays.asList("Test", "MVC");
        return "index";
    }
}
