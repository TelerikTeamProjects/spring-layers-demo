package com.company.springlayersdemo.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Employee {
    @NotNull
    @Min(1)
    private int id;

    @NotNull
    @Size(min = 3,  max = 20, message = "First name should be between 3 and 15 characters.")
    private String firstName;

    @NotNull
    @Size(min = 2,  max = 20, message = "Last name should be between 2 and 15 characters.")
    private String lastName;

    private List<Project> employeeProjects;

    public Employee() {
        employeeProjects = new ArrayList<>();
    }

    public Employee(int id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        employeeProjects = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Project> getEmployeeProjects() {
        return employeeProjects;
    }

    public void setEmployeeProjects(List<Project> employeeProjects) {
        this.employeeProjects = employeeProjects;
    }
}
