package com.company.springlayersdemo.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Project {
    @NotNull
    @Min(0)
    private int id;
    @Size(min = 3,  max = 20, message = "Project name should be between 3 and 20 characters.")
    private String name;

    private List<Employee> projectMembers;

    public Project() {
        projectMembers = new ArrayList<>();
    }

    public Project(int id, String name) {
        this.id = id;
        this.name = name;
        projectMembers = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Employee> getProjectMembers() {
        return projectMembers;
    }

    public void setProjectMembers(List<Employee> projectMembers) {
        this.projectMembers = projectMembers;
    }
}
