package com.company.springlayersdemo.mockobjects;

import com.company.springlayersdemo.models.Employee;
import com.company.springlayersdemo.models.Project;
import com.company.springlayersdemo.repositories.MainRepository;
import com.company.springlayersdemo.services.EmployeeServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceImplTests {
    @Mock
    private MainRepository mockMainRepository;

    @InjectMocks
    private EmployeeServiceImpl service;

    @Test
    public void create_Should_CallRepositoryCreate_When_EmployeeNameIsUnique() {
        // Arrange
        Employee newEmployee = new Employee(1, "Name1", "Family1");

        // Act
        service.create(newEmployee);

        // Assert
        Mockito.verify(mockMainRepository, Mockito.times(1)).create(newEmployee);
        //Mockito.verify(mockMainRepository, Mockito.times(1)).create(Mockito.any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_EmployeeNameIsNotUnique() {
        // Arrange
        Employee newEmployee = new Employee(1, "Name1", "Family1");
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1"),
                        new Employee(2, "Name2", "Family2"),
                        new Employee(3, "Name3", "Family3")
                ));

        // Act
        service.create(newEmployee);

        // Assert
        Mockito.verify(mockMainRepository, Mockito.never()).create(newEmployee);
    }

    @Test
    public void getFilteredEmployees_Should_ReturnEmployeeList_When_FiltersNotExist() {
        // Arrange
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1"),
                        new Employee(2, "Name1", "Family2"),
                        new Employee(3, "Name3", "Family3")
                ));

        Map<String, String> filters = new HashMap<>();
        filters.put("firstName", null);
        filters.put("lastName", null);

        // Act
        List<Employee> result = service.getFilteredEmployees(filters);

        // Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getFilteredEmployees_Should_ReturnEmployeeList_When_FirstNameFilterMatch() {
        // Arrange
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1"),
                        new Employee(2, "Name1", "Family2"),
                        new Employee(3, "Name3", "Family3")
                ));

        Map<String, String> filters = new HashMap<>();
        filters.put("firstName", "Name1");
        filters.put("lastName", null);

        // Act
        List<Employee> result = service.getFilteredEmployees(filters);

        // Assert
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void getFilteredEmployees_Should_ReturnEmployeeList_When_LastNameFilterMatch() {
        // Arrange
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1"),
                        new Employee(2, "Name1", "Family2"),
                        new Employee(3, "Name3", "Family3")
                ));

        Map<String, String> filters = new HashMap<>();
        filters.put("firstName", null);
        filters.put("lastName", "Family2");

        // Act
        List<Employee> result = service.getFilteredEmployees(filters);

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getFilteredEmployees_Should_ReturnEmployeeList_When_FirstAndLastNameFiltersMatch() {
        // Arrange
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1"),
                        new Employee(2, "Name1", "Family2"),
                        new Employee(3, "Name3", "Family3")
                ));

        Map<String, String> filters = new HashMap<>();
        filters.put("firstName", "Name1");
        filters.put("lastName", "Family2");

        // Act
        List<Employee> result = service.getFilteredEmployees(filters);

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getFilteredEmployees_Should_ReturnEmptyEmployeeList_When_FiltersNotMatch() {
        // Arrange
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1"),
                        new Employee(2, "Name1", "Family2"),
                        new Employee(3, "Name3", "Family3")
                ));

        Map<String, String> filters = new HashMap<>();
        filters.put("firstName", "Test Name");
        filters.put("lastName", "Test Family");

        // Act
        List<Employee> result = service.getFilteredEmployees(filters);

        // Assert
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getById_Should_ReturnEmployee_When_MatchExist() {
        // Arrange
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1"),
                        new Employee(2, "Name2", "Family2"),
                        new Employee(3, "Name3", "Family3")
                ));

        // Act
        Employee result = service.getById(2);

        // Assert
        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = ResponseStatusException.class)
    public void getById_Should_ThrowException_When_MatchNotExist() {
        // Arrange
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1"),
                        new Employee(2, "Name2", "Family2"),
                        new Employee(3, "Name3", "Family3")
                ));

        // Act
        service.getById(4);
    }

    @Test
    public void update_Should_ReturnUpdatedEmployee_When_EmployeeNamesAreDifferent() {
        // Arrange
        Employee newEmployee = new Employee(1, "New Name", "New Family");
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1")
                ));

        Mockito.when(mockMainRepository.update(service.getById(1), newEmployee))
                .thenReturn(
                        new Employee(1, "New Name", "New Family")
                );

        // Act
        Employee result = service.update(1, newEmployee);

        // Assert
        Assert.assertEquals("New Name", result.getFirstName());
        Assert.assertEquals("New Family", result.getLastName());
    }

    @Test(expected = ResponseStatusException.class)
    public void update_Should_ThrowException_When_EmployeeNotExist() {
        // Arrange
        Employee newEmployee = new Employee(2, "New Name", "New Family");
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1")
                ));

        Mockito.when(mockMainRepository.update(service.getById(2), newEmployee))
                .thenReturn(
                        new Employee(1, "New Name", "New Family")
                );

        // Act
        Employee result = service.update(1, newEmployee);
    }


    @Test
    public void addProject_Should_AddProjectToProjectLists_When_ExistingProjectIsAdded() {
        // Arrange
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1"),
                        new Employee(2, "Name2", "Family2"),
                        new Employee(3, "Name3", "Family3")
                ));

        Mockito.when(mockMainRepository.getAllProjects())
                .thenReturn(Arrays.asList(
                        new Project(1, "Project 1"),
                        new Project(2, "Project 2")
                ));

        // Act
        Employee employeeToUpdate = service.addProject(1, 1);

        // Assert
        Assert.assertEquals(1, employeeToUpdate.getEmployeeProjects().size());
        Assert.assertEquals(1, mockMainRepository.getAllProjects().get(0).getProjectMembers().size());
    }

    @Test(expected = ResponseStatusException.class)
    public void addProject_Should_ThrowException_When_ProjectNotExist() {
        // Arrange
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1"),
                        new Employee(2, "Name2", "Family2"),
                        new Employee(3, "Name3", "Family3")
                ));

        Mockito.when(mockMainRepository.getAllProjects())
                .thenReturn(Arrays.asList(
                        new Project(1, "Project 1"),
                        new Project(2, "Project 2")
                ));

        // Act
        service.addProject(1, 3);
    }

    @Test
    public void removeProject_Should_RemoveProjectFromProjectLists_When_ExistingProjectIsRemoved() {
        // Arrange
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1")
                ));

        Mockito.when(mockMainRepository.getAllProjects())
                .thenReturn(Arrays.asList(
                        new Project(1, "Project 1")
                ));

        Employee employeeToUpdate = service.addProject(1, 1);

        // Act
        service.removeProject(employeeToUpdate.getId(), 1);

        // Assert
        Assert.assertEquals(0, mockMainRepository.getAllEmployees().get(0).getEmployeeProjects().size());
        Assert.assertEquals(0, mockMainRepository.getAllProjects().get(0).getProjectMembers().size());
    }

    @Test(expected = ResponseStatusException.class)
    public void removeProject_Should_ThrowException_When_ProjectNotExist() {
        // Arrange
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1")
                ));

        Mockito.when(mockMainRepository.getAllProjects())
                .thenReturn(Arrays.asList(
                        new Project(1, "Project 1")
                ));

        Employee employeeToUpdate = service.addProject(1, 1);

        // Act
        service.removeProject(employeeToUpdate.getId(), 2);
    }

    @Test
    public void delete_Should_DeleteEmployeeAndUpdateRelatedObjects_When_EmployeeIsDeleted() {
        // Arrange
        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        new Employee(1, "Name1", "Family1"),
                        new Employee(2, "Name2", "Family2")
                ));

        Mockito.when(mockMainRepository.getAllProjects())
                .thenReturn(Arrays.asList(
                        new Project(1, "Project 1"),
                        new Project(2, "Project 2")
                ));

        Employee employeeToDelete = service.addProject(1, 1);
        employeeToDelete = service.addProject(1, 2);

        // Act
        service.delete(employeeToDelete.getId());

        // Assert
        Mockito.verify(mockMainRepository, Mockito.times(1)).delete(employeeToDelete);
        //Assert.assertEquals(1, mockMainRepository.getAllEmployees().size());
        Assert.assertEquals(0, mockMainRepository.getAllProjects().get(0).getProjectMembers().size());
        Assert.assertEquals(0, mockMainRepository.getAllProjects().get(1).getProjectMembers().size());
    }
}
