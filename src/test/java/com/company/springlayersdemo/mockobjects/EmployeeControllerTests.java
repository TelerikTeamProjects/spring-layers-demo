package com.company.springlayersdemo.mockobjects;

import com.company.springlayersdemo.models.Employee;
import com.company.springlayersdemo.services.EmployeeService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.isA;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTests {
    @MockBean
    EmployeeService mockService;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void new_Should_ReturnStatusBadRequest_When_InvalidIdIsPassed() throws Exception {
        // Arrange
        String employeeInJson = "{\"id\":0,\"firstName\":\"First Name\",\"lastName\":\"Last Name\"}";

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/employees/new")
                .content(employeeInJson)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        // Mockito.verify(mockService, times(0)).create(employee);
    }


    @Test
    public void new_Should_ReturnStatusBadRequest_When_FirstNameIsShorter() throws Exception {
        // Arrange
        String employeeInJson = "{\"id\":1,\"firstName\":\"FN\",\"lastName\":\"The First\"}";

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/employees/new")
                .content(employeeInJson)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void new_Should_ReturnStatusBadRequest_When_FirstNameIsLonger() throws Exception {
        // Arrange
        String employeeInJson = "{\"id\":1,\"firstName\":\"First Name is too loooooooooooong\",\"lastName\":\"The First\"}";

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/employees/new")
                .content(employeeInJson)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void new_Should_ReturnStatusBadRequest_When_LastNameIsShorter() throws Exception {
        // Arrange
        String employeeInJson = "{\"id\":1,\"firstName\":\"First Name\",\"lastName\":\"L\"}";

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/employees/new")
                .content(employeeInJson)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void new_Should_ReturnStatusBadRequest_When_LastNameIsLonger() throws Exception {
        // Arrange
        String employeeInJson = "{\"id\":1,\"firstName\":\"First Name\",\"lastName\":\"Last Name is too loooooooooooong\"}";

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/employees/new")
                .content(employeeInJson)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    //@Ignore //cannot mock it
    @Test
    public void new_Should_ReturnStatusNotFound_When_EmployeeExists() throws Exception {
        // Arrange
        Employee employee = new Employee(1, "First Name", "Last Name");
        Mockito.doThrow(IllegalArgumentException.class).when(mockService).create(isA(Employee.class));
        String employeeInJson = "{\"id\":1,\"firstName\":\"First Name\",\"lastName\":\"Last Name\"}";

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/employees/new")
                .content(employeeInJson)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    // TODO Add additional validations
    @Test
    public void new_Should_ReturnStatusOK_When_ValidRequestBodyIsPassed() throws Exception {
        // Arrange
        String employeeInJson = "{\"id\":1,\"firstName\":\"First Name\",\"lastName\":\"Last Name\"}";
        // Employee employee = new Employee(0, "First Name", "Last Name");

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/employees/new")
                .content(employeeInJson)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getById_Should_ReturnStatusOK_When_EmployeeExists() throws Exception {
        // Arrange
        Mockito.when(mockService.getById(1))
                .thenReturn(new Employee(1, "First Name", "Last Name"));

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/employees/{1}", 1))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value("First Name"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value("Last Name"));
    }

    @Test
    public void getById_Should_ReturnStatusNotFound_When_EmployeeNotExists() throws Exception {
        // Arrange
        Mockito.when(mockService.getById(1))
                .thenThrow(IllegalArgumentException.class);

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/employees/{1}", 1))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void getEmployees_Should_ReturnStatusOK_When_EmptyListIsReturned() throws Exception {
        // Arrange
        Map<String, String> filters = new HashMap<>();
        filters.put("firstName", null);
        filters.put("lastName", null);

        Mockito.when(mockService.getFilteredEmployees(filters))
                .thenReturn(Arrays.asList());

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getEmployees_Should_ReturnStatusOK_When_NoFiltersApplied() throws Exception {
        // Arrange
        Map<String, String> filters = new HashMap<>();
        filters.put("firstName", null);
        filters.put("lastName", null);

        Mockito.when(mockService.getFilteredEmployees(filters))
                .thenReturn(Arrays.asList(new Employee(1, "First Name", "Last Name")));

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1));
    }

    @Test
    public void getEmployees_Should_ReturnStatusOK_When_FirstNameFilterIsApplied() throws Exception {
        // Arrange
        Map<String, String> filters = new HashMap<>();
        filters.put("firstName", "First Name");
        filters.put("lastName", null);

        Mockito.when(mockService.getFilteredEmployees(filters))
                .thenReturn(Arrays.asList(new Employee(1, "First Name", "Last Name")));

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/employees")
                .param("firstName", "First Name"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].firstName").value("First Name"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].lastName").value("Last Name"));
    }

    @Test
    public void getEmployees_Should_ReturnStatusOK_When_LastNameFilterIsApplied() throws Exception {
        // Arrange
        Map<String, String> filters = new HashMap<>();
        filters.put("firstName", null);
        filters.put("lastName", "Last Name");

        Mockito.when(mockService.getFilteredEmployees(filters))
                .thenReturn(Arrays.asList(new Employee(1, "First Name", "Last Name")));

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/employees")
                .param("lastName", "Last Name"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].firstName").value("First Name"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].lastName").value("Last Name"));
    }

    @Test
    public void getEmployees_Should_ReturnStatusOK_When_AllFiltersApplied() throws Exception {
        // Arrange
        Map<String, String> filters = new HashMap<>();
        filters.put("firstName", "First Name");
        filters.put("lastName", "Last Name");

        Mockito.when(mockService.getFilteredEmployees(filters))
                .thenReturn(Arrays.asList(new Employee(1, "First Name", "Last Name")));

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/employees")
                .param("firstName", "First Name")
                .param("lastName", "Last Name"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].firstName").value("First Name"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].lastName").value("Last Name"));
    }

    // TODO validate through Postman
    @Test
    public void update_Should_ReturnStatusOK_When_EmployeeExists() throws Exception {
        // Arrange
        String employeeInJson = "{\"id\":1,\"firstName\":\"New Name\",\"lastName\":\"New Family\"}";
        Employee employee = new Employee(1, "New Name", "New Family");

        Mockito.when(mockService.update(1, employee))
                .thenReturn(new Employee(1, "New Name", "New Family"));

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/employees/{id}", 1)
                .content(employeeInJson)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    // TODO doesnt work
    @Ignore
    @Test
    public void update_Should_ReturnStatusNotFound_When_EmployeeNotExists() throws Exception {
        // Arrange
        int id = 1;
        String employeeInJson = "{\"id\":1,\"firstName\":\"New Name\",\"lastName\":\"New Family\"}";
        Employee employee = new Employee(1, "New Name", "New Family");

        Mockito.when(mockService.update(1, employee))
                .thenThrow(IllegalArgumentException.class);

        // Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/employees/" + id)
                .content(employeeInJson)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}

