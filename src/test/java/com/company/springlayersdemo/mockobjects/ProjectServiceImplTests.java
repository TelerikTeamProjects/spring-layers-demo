package com.company.springlayersdemo.mockobjects;

import com.company.springlayersdemo.models.Employee;
import com.company.springlayersdemo.models.Project;
import com.company.springlayersdemo.repositories.MainRepository;
import com.company.springlayersdemo.services.EmployeeServiceImpl;
import com.company.springlayersdemo.services.ProjectServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ProjectServiceImplTests {
    @Mock
    private MainRepository mockMainRepository;

    @InjectMocks
    private ProjectServiceImpl service;

    @Mock
    private EmployeeServiceImpl employeeService;

    @Test
    public void create_Should_CallRepositoryCreate_When_EmployeeNameIsUnique() {
        // Arrange
        Project newProject = new Project(1, "Project 1");

        // Act
        service.create(newProject);

        // Assert
        Mockito.verify(mockMainRepository, Mockito.times(1)).create(newProject);
    }

    @Test
    public void getAllEmployees_Should_ReturnProjetcList_When_GetAllMethodIsCalled() {
        // Arrange
        Mockito.when(mockMainRepository.getAllProjects())
                .thenReturn(Arrays.asList(
                        new Project(1, "Project 1"),
                        new Project(2, "Project 2"),
                        new Project(3, "Project 3")
                ));

        // Act
        List<Project> result = service.getAll();

        // Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getById_Should_ReturnProject_When_MatchExist() {
        // Arrange
        Mockito.when(mockMainRepository.getAllProjects())
                .thenReturn(Arrays.asList(
                        new Project(1, "Project 1"),
                        new Project(2, "Project 2"),
                        new Project(3, "Project 3")
                ));

        // Act
        Project result = service.getById(2);

        // Assert
        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = ResponseStatusException.class)
    public void getById_Should_ThrowException_When_MatchNotExist() {
        // Arrange
        Mockito.when(mockMainRepository.getAllProjects())
                .thenReturn(Arrays.asList(
                        new Project(1, "Project 1"),
                        new Project(2, "Project 2"),
                        new Project(3, "Project 3")
                ));

        // Act
        service.getById(4);
    }

    @Test
    public void update_Should_ReturnUpdatedProject_When_ProjectNameIsDifferent() {
        // Arrange
        Project newProject = new Project(1, "New Project");
        Mockito.when(mockMainRepository.getAllProjects())
                .thenReturn(Arrays.asList(
                        new Project(1, "Project 1")
                ));

        Mockito.when(mockMainRepository.update(service.getById(1), newProject))
                .thenReturn(
                        new Project(1, "New Project")
                );

        // Act
        Project result = service.update(1, newProject);

        // Assert
        Assert.assertEquals("New Project", result.getName());
    }

    @Test
    public void delete_Should_DeleteProjectAndUpdateRelatedObjects_When_ProjectIsDeleted() {
        // Arrange
        Employee firstEmployee = new Employee(1, "Name1", "Family1");
        Employee secondEmployee = new Employee(2, "Name2", "Family2");
        Project firstProject = new Project(1, "Project 1");
        firstEmployee.getEmployeeProjects().add(firstProject);
        secondEmployee.getEmployeeProjects().add(firstProject);
        firstProject.getProjectMembers().add(firstEmployee);
        firstProject.getProjectMembers().add(secondEmployee);

        Mockito.when(mockMainRepository.getAllEmployees())
                .thenReturn(Arrays.asList(
                        firstEmployee,
                        secondEmployee
                ));

        Mockito.when(mockMainRepository.getAllProjects())
                .thenReturn(Arrays.asList(
                        firstProject
                ));

        // Act
        service.delete(firstProject.getId());

        // Assert
        Mockito.verify(mockMainRepository, Mockito.times(1)).delete(firstProject);
    }
}
